/*

2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

*/
function gcd(a,b){
	var remainder;
	if (a>b) {
	while (remainder!=0) {
		remainder = a % b;
		a = b;
		b = remainder;
	}
	return a
	} else {
		while (remainder!=0) {
			remainder = b % a;
			b = a;
			a = remainder;
		}
	return b;
	}
}
function mostCompositeNumber(n) {
	var compositeNumber = 1;
	for (i=2; i <n+1; i++) {
		compositeNumber *= (i/gcd(compositeNumber,i));
		//console.log(i/gcd(compositeNumber,i));
	}
	return compositeNumber;
}

console.log(mostCompositeNumber(20));
