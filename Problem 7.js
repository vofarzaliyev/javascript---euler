/*
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 10 001st prime number?

*/

function isPrime(n){
	if (n == 1) {
		return false
	}
	if (n==2) {
		return true
	}
	var i;
	if (n>2) {
		for (i=2; i < Math.ceil(n**0.5)+1; i++) {
			//console.log(i);
			if (n % i === 0) {
				return false;
			}
		}
	}
	return true;
}
console.log(isPrime(3));
function *primeRange(start,end) {
	var i;
	for (i=start; i<end+1; i++) {
		//console.log(i);
		if (isPrime(i)) {
			yield i;
		}
	}
}
console.log(primeRange(1,10));
function nthPrime(n){
	var primes = primeRange(1,n**2)
	var ithPrime;
	var i;
	for (i=0; i<n; i++) {
		ithPrime = primes.next().value;
	}
	return ithPrime;
}

console.log(nthPrime(10001));

/*
console.log(nthPrime(1)) returns 'undefined' rather than 2. Look into that problem

*/