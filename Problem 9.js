/*
A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,

a2 + b2 = c2
For example, 32 + 42 = 9 + 16 = 25 = 52.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc
*/

function pyhtagorianTripletSumToN(n){
	var triplets = [];

	for (i=n; i>0; --i) {
		for (j=1; j<n; j++) {
			if (i**2+j**2==(n-i-j)**2) {
				triplets.push([i,j,n-(i+j)]);
			}
		}
	}
	return triplets;
}

console.log(pyhtagorianTripletSumToN(12));