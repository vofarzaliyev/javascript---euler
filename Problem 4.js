/*
A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.

*/

function isPalindrome(n) {
	if (n.toString() === n.toString().split('').reverse().join('')) {
		return true;
	}
	return false;
}
function palindromGeneratorFromAbove() {
	var palindromes = [];
	for (i=1000; i>99; --i) {
		for (j=i; j>99; --j){
			console.log([i,j]);
			if (isPalindrome(i*j)) {
				palindromes.push(i*j);

			}
		}
	}
	return Math.max.apply(null,palindromes);
}

console.log(palindromGeneratorFromAbove());
