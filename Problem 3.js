/*
The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?

*/

function isPrime(n){
	if (n == 1) {
		return true
	}
	var i;
	for (i=2; i < Math.ceil(n**0.5); i++) {
		//console.log(i);
		if (n % i === 0) {
			return false;
		}
	}
	return true;
}

function *primeRange(start,end) {
	var i;
	for (i=start; i<end; i++) {
		if (isPrime(i)) {
			yield i;
		}
	}
}

function findFactors(n) {
	var factors = [];
	var primes = primeRange(2,Math.ceil(n**0.5));
	var divisor;
	do {
		divisor = primes.next().value;
		if (n % divisor == 0) {
			factors.push(divisor);
		} 
	} while (divisor < n);
	return factors[factors.length -1];
}


console.log(findFactors(600851475143));
console.log(isPrime(486847))
/*
var primes = primeRange(10,30);
var elebele;
elebele = primes.next().value;
console.log(elebele);
console.log(elebele);
console.log(elebele);
console.log(primes.next().value);
console.log(primes.next().value);
console.log(primes.next().value);
console.log(primes.next().value);
*/