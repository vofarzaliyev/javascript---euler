/*
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.

*/

function isPrime(n){
	if (n == 1) {
		return true
	}
	if (n==2) {
		return true
	}
	var i;
	if (n>2) {
		for (i=2; i < Math.ceil(n**0.5)+1; i++) {
			//console.log(i);
			if (n % i === 0) {
				return false;
			}
		}
	}
	return true;
}
console.log(isPrime(4));
function *primeRange(start,end) {
	var i;
	for (i=start; i<end+1; i++) {
		//console.log(i);
		if (isPrime(i)) {
			yield i;
		}
	}
}
function sumOfPrimesBelowN(n) {
	var primes = primeRange(2,n+1);
	var value = 0;
	var prime = 0;
	while (prime<n) {
		value += prime;
		prime = primes.next().value;

	}
	return value;
	}
console.log(sumOfPrimesBelowN(10));
console.log(sumOfPrimesBelowN(2000000));
